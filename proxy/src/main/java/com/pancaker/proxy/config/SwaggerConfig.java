package com.pancaker.proxy.config;

import com.pancaker.common.config.SwaggerCommonConfig;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig extends SwaggerCommonConfig {

}
