package com.pancaker.proxy.controller;

import com.pancaker.common.dto.OperationRequest;
import com.pancaker.common.dto.OperationResponse;
import com.pancaker.proxy.service.OperationHystrixCollapser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.ExecutionException;

@Slf4j
@RestController
@RequestMapping("/proxy")
public class ProxyHystrixCollapserController {

    //region init

    private final OperationHystrixCollapser operationHystrixCollapser;

    public ProxyHystrixCollapserController(OperationHystrixCollapser operationHystrixCollapser) {

        this.operationHystrixCollapser = operationHystrixCollapser;
    }
    //endregion


    //region hystrix collapser
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @PostMapping(value = "/add-collapse")
    public OperationResponse getAdditionResultWithCollapser(@RequestBody OperationRequest request) throws ExecutionException, InterruptedException {
        return operationHystrixCollapser.addAsync(request).get();

    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public OperationResponse notFoundResponse(Exception e) {
        log.error("Error from collapsed", e);
        return OperationResponse.builder()
                .type(OperationResponse.OperationType.ERROR).build();
    }
    //endregion
}
