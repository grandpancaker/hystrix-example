package com.pancaker.proxy.controller;

import com.pancaker.common.dto.OperationRequest;
import com.pancaker.common.dto.OperationResponse;
import com.pancaker.proxy.service.OperationHystrixService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/proxy")
public class ProxyHystrixController {

    //region init
    private final OperationHystrixService operationHystrixService;

    public ProxyHystrixController(OperationHystrixService operationHystrixService) {
        this.operationHystrixService = operationHystrixService;
    }
    //endregion

    //region with hystrix
    @PostMapping("/add-with-hystrix")
    public OperationResponse addWithHystrix(@RequestBody OperationRequest request) {
        return operationHystrixService.addWithHystrix(request);
    }

    @PostMapping("/add-with-hystrix-rand-exception")
    public OperationResponse addWithHystrixRandException(@RequestBody OperationRequest request) {
        return operationHystrixService.addWithHystrixRandException(request);
    }

    @PostMapping("/add-with-hystrix-rand-exception-ignored")
    public OperationResponse addWithHystrixRandExceptionIgnored(@RequestBody OperationRequest request) {
        return operationHystrixService.addWithHystrixRandExceptionIgnored(request);
    }

    @PostMapping("/add-always-too-slow")
    public OperationResponse addAlwaysTooSlow(@RequestBody OperationRequest request) {
        return operationHystrixService.addAlwaysTooSlow(request);
    }

    @PostMapping("/add-always-too-slow-different-fall-back")
    public OperationResponse addAlwaysTooSlowDifferentFallBack(@RequestBody OperationRequest request) {
        return operationHystrixService.addAlwaysTooSlowDifferentFallBack(request);
    }
    //endregion

}
