package com.pancaker.proxy.service;

import com.pancaker.common.dto.OperationRequest;
import com.pancaker.common.dto.OperationResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Random;

@Slf4j
@Service
public class OperationRestService {
    private final String ADDITION_URL = "http://localhost:9090/calculate/add";

    private final RestTemplate restTemplate;

    public OperationRestService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public OperationResponse add(OperationRequest request){
        return add(request.getLeftParameter(), request.getRightParameter());
    }

    public OperationResponse add(Float num1, Float num2) {
        var request = OperationRequest.builder()
                .leftParameter(num1)
                .rightParameter(num2)
                .build();


        var url = ADDITION_URL;

        var response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(request), OperationResponse.class);

        log.info("status: {}, code: {}, result: {}",
                response.getStatusCode(),
                response.getStatusCodeValue(),
                response.getBody());

        return response.getBody();
    }

    public OperationResponse addWithRandomFailure(OperationRequest request){
        return addWithRandomFailure(request.getLeftParameter(), request.getRightParameter());
    }

    public OperationResponse addWithRandomFailure(Float num1, Float num2) {
        Random rand = new Random();
        int n = rand.nextInt(100);
        log.info("addition with failure ( {} >= 50)", n);
        if (n >= 50) {
            throw new RuntimeException();
        } else {
            return add(num1, num2);
        }

    }
}
