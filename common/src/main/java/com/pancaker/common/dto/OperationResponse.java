package com.pancaker.common.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OperationResponse {
    public enum OperationType {
        ADD,
        ERROR
    }

    private OperationType type;
    private Float result;
    private LocalDateTime generationTime;
}
